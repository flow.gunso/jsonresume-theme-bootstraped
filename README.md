# JsonResume theme Bootstraped

This is a JsonResume theme that use the Boostrap framework as well as the FontAwesome library!

## Warning

Do not use the theme because:
* This theme does not use the standard JsonResume schema. The schema has been tweaked a bit to improve a few things:
    * Language's `fluency` and Skill's`level` are using integers from _0 to 100_
    * Language and Skill have a `fluencyName` and `levelName` field to ease readability.
    * Project's `url` text field have be dropped in favor of `urls` object. `urls` is list of dictionnaries with the fields `network`, `faType`, `url` and `label`.
* Most of Handlebars `{{#if}}...{{/if}}` from the boilerplate theme have been removed to match my requirements.
* `awards.hbs`, `references.hbs`, `volunteer.hbs` partials have not been customized to use Bootstrap.

## Installation

It can be installed with npm. JsonResume CLI requires it to be installed globally, so:  
`npm install -g https://gitlab.com/flow.gunso/jsonresume-theme-bootstraped`

## Roadmap

* Wait for JsonResume schema to support internationalization.
* Wait for JsonResume to drop Handlebars template engine
* Then I'll think about updating this theme.

## License

Available under the [MIT license](https://opensource.org/licenses/mit-license.php).
