PACKAGE_NAME?=$(shell jq -r .name < package.json)
PACKAGE_VERSION?=$(shell jq -r .version < package.json)

pack:
	npm pack

install: pack
	sudo npm install -g $(PACKAGE_NAME)-$(PACKAGE_VERSION).tgz
	rm -f $(PACKAGE_NAME)-$(PACKAGE_VERSION).tgz
